<?php
session_start();
if (!empty($_GET['phpinfo'])) {
    phpinfo();
}
require_once '../src/lib/vendor/autoload.php';
require_once '../src/config/routing.php';
require_once '../src/models/_classes.php';
require_once '../src/controllers/_controllers.php';
require_once '../src/config/parametres.php';
require_once '../src/app/connexion.php';
require_once '../src/config/fonction.php';
$db = connect($config);
$loader = new Twig_Loader_Filesystem('../src/views/');
$twig = new Twig_Environment($loader, array());
$twig->addExtension(new Twig_Extensions_Extension_Intl());
$twig->addGlobal('session', $_SESSION);
$twig->addGlobal('get', $_GET);

$contenu = getPage($db);
$contenu($twig, $db);
?>
