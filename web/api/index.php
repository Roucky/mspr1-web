<?php
header("Content-Type:application/json");
require_once '../../src/config/parametres.php';
require_once '../../src/app/connexion.php';
require_once '../../src/config/fonction.php';
require_once '../../src/models/_classes.php';
$db = connect($config);

$response = array();
$response['status'] = "1";
$response['text'] = "No issue";
$data = file_get_contents("php://input");
$json = json_decode($data);

if ($db != null) {

    //// LOGIN ////
    if (isset($_POST['emailLogin']) && isset($_POST['passwordLogin'])) {
        $account = new Account($db);
        $accountExist = $account->getByEmail($_POST['emailLogin']);
        $response['account'] = null;
        if (!empty($accountExist)) {
            if ($accountExist['Password'] != $_POST['passwordLogin']) {
                $response['text'] = "The passwords don't match.";
            } else {
                $response['text'] = "Login done.";
                $response['status'] = "0";
                $response['account'] = $accountExist;
            }
        } else {
            $response['text'] = "The account " . $_POST['emailLogin'] . " is unknown.";
        }
    }

    //// REGISTER ////
    if (isset($_POST['name']) && isset($_POST['firstname']) && isset($_POST['email']) && isset($_POST['password'])) {
        $account = new Account($db);
        $accountExist = $account->getByEmail($_POST['email']);
        $response['account'] = null;
        if (!empty($_POST['name']) && !empty($_POST['firstname']) && !empty($_POST['email']) && !empty($_POST['password'])) {
            if (empty($accountExist)) {
                if ($account->insert($_POST['name'], $_POST['firstname'], $_POST['email'], $_POST['password'])) {
                    $response['text'] = "Registration done.";
                    $response['status'] = "0";
                    $response['account'] = $account->getByEmail($_POST['email']);
                } else {
                    $response['text'] = "An error occured.";
                }
            } else {
                $response['text'] = "The account " . $_POST['email'] . " is already in database.";
            }
        } else {
            $response['text'] = "Fields can't be empty.";
        }
    }

    //// DELETE ACCOUNT ////
    if (isset($_POST['emailDelete'])) {
        $account = new Account($db);
        $accountExist = $account->getByEmail($_POST['emailDelete']);
        if (!empty($accountExist)) {
            if ($account->forceDelete($accountExist['Id'])) {
                $response['text'] = "Account deleting done.";
                $response['status'] = "0";
            } else {
                $response['text'] = "An error occured.";
            }
        } else {
            $response['text'] = "The account " . $_POST['emailDelete'] . " is not in database.";
        }
    }

    //// UPDATE ACCOUNT ////
    if (isset($_POST['id']) && isset($_POST['name']) && isset($_POST['firstname']) && isset($_POST['email'])) {
        $account = new Account($db);
        $accountExist = $account->get($_POST['id']);
        $response['account'] = null;
        if (!empty($accountExist)) {
            if ($account->update($_POST['id'], $_POST['name'], $_POST['firstname'], $_POST['email'])) {
                $response['text'] = "Update done.";
                $response['status'] = "0";
                $response['account'] = $account->get($_POST['id']);
            } else {
                $response['text'] = "An error occured.";
            }
        } else {
            $response['text'] = "The account " . $_POST['email'] . " isn't in database.";
        }
    }

    //// QR CODE ////

    if (isset($_POST['qrcode']) && isset($_POST['token'])) {
        $qrcode = new Qrcode($db);
        $couponqrcode = new Couponqrcode($db);
        $accountcoupon = new Accountcoupon($db);
        $result = $qrcode->get($_POST['qrcode']);
        $response['coupons'] = array();
        if (empty($result)) {
            $response['text'] = "QR Code unkown.";
        } else {
            $error = false;
            $nb = 0;
            $coupons = $couponqrcode->findCouponByQrcode($_POST['qrcode']);
            if (count($coupons) > 0) {
                for ($coupon = 0; $coupon < count($coupons); $coupon++) {
                    if (empty($accountcoupon->getByAccountAndCoupon($_POST['token'], $coupons[$coupon]['Id']))) {
                        $accountcoupon->insert($_POST['token'], $coupons[$coupon]['Id']);
                        $nb++;
                    } else {
                        $errorMessage = "Some coupon are already in your coupon list.";
                    }
                }
                $response['coupons'] = $coupons;
            } else {
                $errorMessage = "No coupon associated.";
            }
            $response['status'] = "0";
            if ($error) {
                $response['text'] = $errorMessage;
            } else {
                $response['text'] = "The coupons(x" . $nb . ") are added to the account.";
            }
        }
    }

    //// LIST OF COUPON ////

    if (isset($_GET['token'])) {
        $account = new Account($db);
        $accountResult = $account->get($_GET['token']);
        if (empty($accountResult)) {
            $response['status'] = "1";
            $response['text'] = "Account Does Not Exist";
            $response['coupons'] = array();
        } else {
            $accountcoupon = new Accountcoupon($db);
            $coupons = $accountcoupon->findByAccount($_GET['token']);
            $response['status'] = "1";
            if (count($coupons) > 0) {
                $response['text'] = "All coupon are downloaded.";
                $response['coupons'] = ($coupons);
            } else {
                $response['text'] = "You don't have any coupon.";
                $response['coupons'] = array();
            }
        }
    }
} else {
    $response['text'] = "Error while platform try to connect to database.";
}

echo json_encode($response);
