$(document).ready(function(){
    $('a.see-more').click(function(){
        var caretI = $(this).children('.fa');
        var caretDiv = $(this).siblings('.box-details');

        if(caretI.hasClass('fa-caret-down')){
            caretI.removeClass('fa-caret-down');
            caretI.addClass('fa-caret-up');
            caretDiv.removeClass('d-none');
        }
        else{
            caretI.removeClass('fa-caret-up');
            caretI.addClass('fa-caret-down');
            caretDiv.addClass('d-none');
        }
    });
});