<?php
function getPage($db) {
    $lesPages['accueil'] = "actionAccueil;1";
    $lesPages['account'] = "actionAccount;1";
    $lesPages['accountInsert'] = "actionAccountInsert;1";
    $lesPages['accountUpdate'] = "actionAccountUpdate;1";
    $lesPages['coupon'] = "actionCoupon;1";
    $lesPages['couponInsert'] = "actionCouponInsert;1";
    $lesPages['couponUpdate'] = "actionCouponUpdate;1";
    $lesPages['qrcode'] = "actionQrcode;1";
    $lesPages['qrcodeInsert'] = "actionQrcodeInsert;1";
    $lesPages['qrcodeUpdate'] = "actionQrcodeUpdate;1";
    $lesPages['accountcoupon'] = "actionAccountcoupon;1";
    $lesPages['couponqrcode'] = "actionCouponqrcode;1";
    $lesPages['couponqrcode'] = "actionCouponqrcode;1";
    $lesPages['login'] = "actionLogin;0";
    $lesPages['loginWs'] = "actionLoginWs;0";
    $lesPages['logout'] = "actionLogout;0";
    $lesPages['maintenance'] = "actionMaintenance;0";

    if ($db != null) {
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 'accueil';
        }
        if (!isset($lesPages[$page])) {
            $page = 'accueil';
        }
    } else {
        $page = 'maintenance';
    }
    $explose = explode(';', $lesPages[$page]);
    $role = $explose[1];
    if ($role != 0) {
        if (isset($_SESSION['id'])) {
            if ($role != $_SESSION['role']) {
                $contenu = 'actionLogin';
            } else {
                $contenu = $explose[0];
            }
        } else {
            $contenu = 'actionLogin';
        }
    } else {
        $contenu = $explose[0];
    }
    return $contenu;
}

?>
