<?php

date_default_timezone_set('Europe/Paris');

function date_ago($date) {
    $strTime = array("seconde", "minute", "heure", "jour", "mois", "an");
    $length = array("60", "60", "24", "30", "12", "10");

    $currentTime = time();
    if ($currentTime >= $date) {
        $diff = time() - $date;
        for ($i = 0; $diff >= $length[$i] && $i < count($length) - 1; $i++) {
            $diff = $diff / $length[$i];
        }
        $diff = round($diff);
        $pluriel = '';
        if ($diff > 1 && $strTime[$i] != "mois") {
            $pluriel = 's';
        }
        return "il y a " . $diff . " " . $strTime[$i] . $pluriel;
    }
}

function print_pre($var) {
    echo '<pre style="text-align: left;">';
    print_r($var);
    echo '</pre>';
}

function uploadImg($dir, $file) {
    $imageFileType = explode('.', $file["name"]);
    $imageFileType = strtolower(end($imageFileType));
    $filename = uniqid() . '.' . $imageFileType;
    $target_file = $dir . $filename;
    $upload = true;

    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
        $upload = false;
    }
    if ($upload) {
        if (move_uploaded_file($file["tmp_name"], $target_file)) {
            return $filename;
        }
        return false;
    }
}

function generateCode() {
    $letters = range('A', 'Z');
    $number = range('0', '9');
    $code = "";
    for($i=0;$i<6;$i++) {
        if(rand(1,2) == 1) {
            $code .= $letters[rand(0,count($letters)-1)];
        }
        else {
            $code .= $number[rand(0,count($number)-1)];
        }
    }
    return $code;
}

function returnToIndex() {
    Header('Location: index.php');
}