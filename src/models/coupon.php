<?php
Class Coupon {

    private $db;
    private $find;
    private $get;
    private $count;
    private $insert;
    private $update;
    private $delete;

    public function __construct($db) {
        $this->db = $db;
        $this->find = $db->prepare("SELECT * FROM mspr1_coupon WHERE Deleted='0' ORDER BY Libelle");
        $this->get = $db->prepare("SELECT * FROM mspr1_coupon WHERE Id=:id AND Deleted='0'");
        $this->count = $db->prepare("SELECT COUNT(*) AS count FROM mspr1_coupon WHERE Deleted='0'");
        $this->insert = $db->prepare("INSERT INTO mspr1_coupon VALUES (:id, :libelle, :description, :color, :date, '0')");
        $this->update = $db->prepare("UPDATE mspr1_coupon SET Name=:name WHERE Id=:id");
        $this->delete = $db->prepare("UPDATE mspr1_coupon SET Deleted='1' WHERE Id=:id");
    }

    public function find() {
        $this->find->execute();
        return $this->find->fetchAll(PDO::FETCH_ASSOC);
    }

    public function get($id) {
        $this->get->execute(array(':id' => $id));
        return $this->get->fetch(PDO::FETCH_ASSOC);
    }

    public function count() {
        $this->count->execute();
        $count = $this->count->fetch(PDO::FETCH_ASSOC);
        return $count['count'];
    }

    public function insert($libelle, $description, $color, $date) {
        $this->insert->execute(array(':id' => uniqid(), ':libelle' => $libelle, ':description' => $description, ':color' => $color, ':date' => $date));
        if(($this->insert->ErrorCode() != "00000")) {
            return false;
        }
        return true;
    }

    public function update($id, $libelle, $description, $date) {
        $this->update->execute(array(':id' => $id, ':libelle' => $libelle, ':description' => $description, ':date' => $date));
        if(($this->update->ErrorCode() != "00000")) {
            return false;
        }
        return true;
    }

    public function delete($id) {
        $this->delete->execute(array(':id' => $id));
        if(($this->delete->ErrorCode() != "00000")) {
            return false;
        }
        return true;
    }

}