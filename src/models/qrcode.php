<?php
Class Qrcode {

private $db;
private $find;
private $get;
private $count;
private $insert;
private $update;
private $delete;

public function __construct($db) {
    $this->db = $db;
    $this->find = $db->prepare("SELECT * FROM mspr1_qrcode WHERE Deleted='0' ORDER BY Code");
    $this->get = $db->prepare("SELECT * FROM mspr1_qrcode WHERE Id=:id AND Deleted='0'");
    $this->count = $db->prepare("SELECT COUNT(*) AS count FROM mspr1_qrcode WHERE Deleted='0'");
    $this->insert = $db->prepare("INSERT INTO mspr1_qrcode VALUES (:id, :code, '', '1', '0')");
    $this->update = $db->prepare("UPDATE mspr1_qrcode SET Code=:code WHERE Id=:id");
    $this->delete = $db->prepare("UPDATE mspr1_qrcode SET Deleted='1' WHERE Id=:id");
}

public function find() {
    $this->find->execute();
    return $this->find->fetchAll(PDO::FETCH_ASSOC);
}

public function get($id) {
    $this->get->execute(array(':id' => $id));
    return $this->get->fetch(PDO::FETCH_ASSOC);
}

public function count() {
    $this->count->execute();
    $count = $this->count->fetch(PDO::FETCH_ASSOC);
    return $count['count'];
}

public function insert($code) {
    $this->insert->execute(array(':id' => uniqid(), ':code' => $code));
    if(($this->insert->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

public function update($id, $code) {
    $this->update->execute(array(':id' => $id, ':code' => $code));
    if(($this->update->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

public function delete($id) {
    $this->delete->execute(array(':id' => $id));
    if(($this->delete->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

}