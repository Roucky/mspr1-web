<?php

Class Couponqrcode {

private $db;
private $find;
private $findCouponByQrcode;
private $get;
private $getByCouponAndQrcode;
private $count;
private $insert;
private $update;
private $delete;

public function __construct($db) {
    $this->db = $db;
    $this->find = $db->prepare("SELECT * FROM mspr1_couponqrcode WHERE Deleted='0' ORDER BY champ");
    $this->find = $db->prepare("SELECT mspr1_couponqrcode.Id, mspr1_qrcode.Code, mspr1_coupon.Libelle FROM mspr1_couponqrcode INNER JOIN mspr1_qrcode ON mspr1_qrcode.Id = mspr1_couponqrcode.QrcodeId INNER JOIN mspr1_coupon ON mspr1_coupon.Id = mspr1_couponqrcode.CouponId");
    $this->findCouponByQrcode = $db->prepare("SELECT mspr1_coupon.Id FROM mspr1_couponqrcode INNER JOIN mspr1_coupon ON mspr1_couponqrcode.CouponId = mspr1_coupon.Id WHERE mspr1_couponqrcode.QrcodeId=:qrcode");
    $this->get = $db->prepare("SELECT * FROM mspr1_couponqrcode WHERE Id=:id AND Deleted='0'");
    $this->getByCouponAndQrcode = $db->prepare("SELECT * FROM mspr1_couponqrcode WHERE CouponId=:coupon AND QrcodeId=:qrcode AND Deleted='0'");
    $this->count = $db->prepare("SELECT COUNT(*) AS count FROM mspr1_couponqrcode WHERE Deleted='0'");
    $this->insert = $db->prepare("INSERT INTO mspr1_couponqrcode VALUES (:id, :coupon, :qrcode, '0')");
    $this->update = $db->prepare("UPDATE mspr1_couponqrcode SET Name=:name WHERE Id=:id");
    $this->delete = $db->prepare("DELETE FROM mspr1_couponqrcode WHERE Id=:id");
}

public function find() {
    $this->find->execute();
    return $this->find->fetchAll(PDO::FETCH_ASSOC);
}

public function findCouponByQrcode($qrcode) {
    $this->findCouponByQrcode->execute(array(':qrcode' => $qrcode));
    return $this->findCouponByQrcode->fetchAll(PDO::FETCH_ASSOC);
}

public function get($id) {
    $this->get->execute(array(':id' => $id));
    return $this->get->fetch(PDO::FETCH_ASSOC);
}

public function getByCouponAndQrcode($qrcode, $coupon) {
    $this->getByCouponAndQrcode->execute(array(':coupon' => $coupon, ':qrcode' => $qrcode));
    return $this->getByCouponAndQrcode->fetch(PDO::FETCH_ASSOC);
}

public function count() {
    $this->count->execute();
    $count = $this->count->fetch(PDO::FETCH_ASSOC);
    return $count['count'];
}

public function insert($qrcode, $coupon) {
    $this->insert->execute(array(':id' => uniqid(), ':qrcode' => $qrcode, ':coupon' => $coupon));
    if(($this->insert->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

public function update($id, $name) {
    $this->update->execute(array(':id' => $id, ':name' => $name));
    if(($this->update->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

public function delete($id) {
    $this->delete->execute(array(':id' => $id));
    if(($this->delete->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

}