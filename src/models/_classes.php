<?php
require_once "account.php";
require_once "coupon.php";
require_once "qrcode.php";
require_once "accountcoupon.php";
require_once "couponqrcode.php";

Class Model {

    private $db;
    private $find;
    private $get;
    private $count;
    private $insert;
    private $update;
    private $delete;
    
    public function __construct($db) {
        $this->db = $db;
        $this->find = $db->prepare("SELECT * FROM table WHERE Deleted='0' ORDER BY champ");
        $this->get = $db->prepare("SELECT * FROM table WHERE Id=:id AND Deleted='0'");
        $this->count = $db->prepare("SELECT COUNT(*) AS count FROM table WHERE Deleted='0'");
        $this->insert = $db->prepare("INSERT INTO table(`id`, `name`) VALUES (:id, :name)");
        $this->update = $db->prepare("UPDATE table SET Name=:name WHERE Id=:id");
        $this->delete = $db->prepare("UPDATE table SET Deleted='1' WHERE Id=:id");
    }

    public function find() {
        $this->find->execute();
        return $this->find->fetchAll();
    }

    public function get($id) {
        $this->get->execute(array(':id' => $id));
        return $this->get->fetch();
    }

    public function count() {
        $this->count->execute();
        $count = $this->count->fetch();
        return $count['count'];
    }

    public function insert($name) {
        $this->insert->execute(array(':id' => uniqid(), ':name' => $name));
        if(($this->insert->ErrorCode() != "00000")) {
            return false;
        }
        return true;
    }

    public function update($id, $name) {
        $this->update->execute(array(':id' => $id, ':name' => $name));
        if(($this->update->ErrorCode() != "00000")) {
            return false;
        }
        return true;
    }

    public function delete($id) {
        $this->delete->execute(array(':id' => $id));
        if(($this->delete->ErrorCode() != "00000")) {
            return false;
        }
        return true;
    }

}

?>
