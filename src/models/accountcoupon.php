<?php

Class Accountcoupon {

private $db;
private $find;
private $get;
private $getByAccountAndCoupon;
private $count;
private $insert;
private $update;
private $delete;

public function __construct($db) {
    $this->db = $db;
    $this->find = $db->prepare("SELECT mspr1_accountcoupon.Id, mspr1_account.Name, mspr1_coupon.Libelle FROM mspr1_accountcoupon INNER JOIN mspr1_account ON mspr1_account.Id = mspr1_accountcoupon.AccountId INNER JOIN mspr1_coupon ON mspr1_coupon.Id = mspr1_accountcoupon.CouponId");
    $this->findByAccount = $db->prepare("SELECT mspr1_coupon.* FROM mspr1_accountcoupon INNER JOIN mspr1_coupon ON mspr1_coupon.Id = mspr1_accountcoupon.CouponId WHERE mspr1_accountcoupon.AccountId=:account");
    $this->get = $db->prepare("SELECT * FROM mspr1_accountcoupon WHERE Id=:id AND Deleted='0'");
    $this->getByAccountAndCoupon = $db->prepare("SELECT * FROM mspr1_accountcoupon WHERE AccountId=:account AND CouponId=:coupon AND Deleted='0'");
    $this->count = $db->prepare("SELECT COUNT(*) AS count FROM mspr1_accountcoupon WHERE Deleted='0'");
    $this->insert = $db->prepare("INSERT INTO mspr1_accountcoupon VALUES (:id, :account, :coupon, '0', '0')");
    $this->update = $db->prepare("UPDATE mspr1_accountcoupon SET Name=:name WHERE Id=:id");
    $this->delete = $db->prepare("DELETE FROM mspr1_accountcoupon WHERE Id=:id");
}

public function find() {
    $this->find->execute();
    return $this->find->fetchAll(PDO::FETCH_ASSOC);
}

public function findByAccount($account) {
    $this->findByAccount->execute(array(':account' => $account));
    return $this->findByAccount->fetchAll(PDO::FETCH_ASSOC);
}

public function get($id) {
    $this->get->execute(array(':id' => $id));
    return $this->get->fetch(PDO::FETCH_ASSOC);
}

public function getByAccountAndCoupon($account, $coupon) {
    $this->getByAccountAndCoupon->execute(array(':account' => $account, ':coupon' => $coupon));
    return $this->getByAccountAndCoupon->fetch(PDO::FETCH_ASSOC);
}

public function count() {
    $this->count->execute();
    $count = $this->count->fetch(PDO::FETCH_ASSOC);
    return $count['count'];
}

public function insert($account, $coupon) {
    $this->insert->execute(array(':id' => uniqid(), ':account' => $account, ':coupon' => $coupon));
    if(($this->insert->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

public function update($id, $name) {
    $this->update->execute(array(':id' => $id, ':name' => $name));
    if(($this->update->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

public function delete($id) {
    $this->delete->execute(array(':id' => $id));
    if(($this->delete->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

}