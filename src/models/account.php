<?php
Class Account {

private $db;
private $find;
private $get;
private $getByEmail;
private $count;
private $insert;
private $update;
private $delete;
private $forceDelete;

public function __construct($db) {
    $this->db = $db;
    $this->find = $db->prepare("SELECT * FROM mspr1_account WHERE Deleted='0' ORDER BY Name");
    $this->get = $db->prepare("SELECT * FROM mspr1_account WHERE Id=:id AND Deleted='0'");
    $this->getByEmail = $db->prepare("SELECT * FROM mspr1_account WHERE Email=:email AND Deleted='0'");
    $this->count = $db->prepare("SELECT COUNT(*) AS count FROM mspr1_account WHERE Deleted='0'");
    $this->insert = $db->prepare("INSERT INTO mspr1_account VALUES (:id, :accountname, :firstname, :email, :accountpassword, :birthday, '', '', '', '', '', '1', '0')");
    $this->update = $db->prepare("UPDATE mspr1_account SET `Name`=:name, `Firstname`=:firstname, `Email`=:email WHERE Id=:id");
    $this->delete = $db->prepare("UPDATE mspr1_account SET Deleted='1' WHERE Id=:id");
    $this->forceDelete = $db->prepare("DELETE FROM mspr1_account WHERE Id=:id");
}

public function find() {
    $this->find->execute();
    return $this->find->fetchAll(PDO::FETCH_ASSOC);
}

public function get($id) {
    $this->get->execute(array(':id' => $id));
    return $this->get->fetch(PDO::FETCH_ASSOC);
}

public function getByEmail($email) {
    $this->getByEmail->execute(array(':email' => $email));
    return $this->getByEmail->fetch(PDO::FETCH_ASSOC);
}

public function count() {
    $this->count->execute();
    $count = $this->count->fetch(PDO::FETCH_ASSOC);
    return $count['count'];
}

public function insert($name, $firstname, $email, $password) {
    $this->insert->execute(array(':id' => uniqid(), ':accountname' => $name, ':firstname' => $firstname, ':email' => $email, ':accountpassword' => $password, ':birthday' => date('Y-m-d')));
    if(($this->insert->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

public function update($id, $name, $firstname, $email) {
    $this->update->execute(array(':id' => $id, ':name' => $name, ':firstname' => $firstname, ':email' => $email));
    if(($this->update->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

public function delete($id) {
    $this->delete->execute(array(':id' => $id));
    if(($this->delete->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

public function forceDelete($id) {
    $this->forceDelete->execute(array(':id' => $id));
    if(($this->forceDelete->ErrorCode() != "00000")) {
        return false;
    }
    return true;
}

}