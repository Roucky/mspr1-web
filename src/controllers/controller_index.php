<?php

function actionAccueil($twig, $db)
{
    $model = array();
    echo $twig->render('index.html.twig', array('model' => $model));
}

//// LOGIN ////

function actionLogin($twig, $db)
{
    $model = array();
    $account = new Account($db);
    if (isset($_POST['email'])) {
        $accountExist = $account->getByEmail($_POST['email']);
        if ($accountExist['role'] == 2) {
            if ($accountExist['Password'] != md5($_POST['password'])) {
                $model['error'] = "Une erreur est survenue, veuillez réessayer.";
            } else {
                $_SESSION['id'] = $accountExist['Id'];
                $_SESSION['role'] = $accountExist['Role'];
                returnToIndex();
            }
        } else {
            $model['error'] = "Vous n'êtes pas autorisé.";
        }
    }
    echo $twig->render('login.html.twig', array('model' => $model));
}

function actionLogout($twig, $db)
{
    session_destroy();
    Header('Location: index.php');
}

//// ACCOUNT ////

function actionAccount($twig, $db)
{
    $model = array();
    $account = new Account($db);
    if (isset($_GET['deleted'])) {
        $model['error'] = array();
        $model['error']['status'] = 1;
        if (!empty($account->get($_GET['deleted']))) {
            if ($account->delete($_GET['deleted'])) {
                $model['error']['status'] = 0;
                $model['error']['text'] = "Le compte a été supprimé.";
            } else {
                $model['error']['text'] = "Une erreur est survenue.";
            }
        } else {
            $model['error']['text'] = "Le compte n'existe pas.";
        }
    }
    $model['accounts'] = $account->find();
    echo $twig->render('account/index.html.twig', array('model' => $model));
}

function actionAccountInsert($twig, $db)
{
    $model = array();
    $account = new Account($db);
    if (isset($_POST['name'])) {
        $model['error'] = array();
        if ($account->insert($_POST['name'], $_POST['firstname'], $_POST['email'], md5($_POST['password']))) {
            $model['error']['status'] = 0;
            $model['error']['text'] = "Le compte a été créé.";
        } else {
            $model['error']['status'] = 1;
            $model['error']['text'] = "Une erreur est survenue.";
        }
    }
    echo $twig->render('account/insert.html.twig', array('model' => $model));
}

function actionAccountUpdate($twig, $db)
{
    $model = array();
    $account = new Account($db);
    $model['error']['status'] = 1;
    if (isset($_GET['id'])) {
        $compte = $account->get($_GET['id']);
        if (empty($compte)) {
            $model['error']['text'] = "Le compte n'existe pas.";
        } else {
            unset($model['error']['status']);
            if (isset($_POST['name'])) {
                if ($account->update($_GET['id'], $_POST['name'], $_POST['firstname'], $_POST['email'])) {
                    $model['error']['status'] = 0;
                    $model['error']['text'] = "Le compte a été modifié.";
                } else {
                    $model['error']['status'] = 1;
                    $model['error']['text'] = "Une erreur est survenue.";
                }
            }
            $model['account'] = $compte;
        }
    } else {
        $model['error']['text'] = "Le compte n'existe pas.";
    }
    echo $twig->render('account/update.html.twig', array('model' => $model));
}

//// COUPON ////

function actionCoupon($twig, $db)
{
    $model = array();
    $coupon = new Coupon($db);
    if (isset($_GET['deleted'])) {
        $model['error'] = array();
        $model['error']['status'] = 1;
        if (!empty($coupon->get($_GET['deleted']))) {
            if ($coupon->delete($_GET['deleted'])) {
                $model['error']['status'] = 0;
                $model['error']['text'] = "Le coupon a été supprimé.";
            } else {
                $model['error']['text'] = "Une erreur est survenue.";
            }
        } else {
            $model['error']['text'] = "Le compte n'existe pas.";
        }
    }
    $model['coupons'] = $coupon->find();
    echo $twig->render('coupon/index.html.twig', array('model' => $model));
}

function actionCouponInsert($twig, $db)
{
    $model = array();
    $coupon = new Coupon($db);
    if (isset($_POST['libelle'])) {
        $model['error'] = array();
        if ($coupon->insert($_POST['libelle'], $_POST['description'], $_POST['colorcode'], $_POST['date'])) {
            $model['error']['status'] = 0;
            $model['error']['text'] = "Le coupon a été créé.";
        } else {
            $model['error']['status'] = 1;
            $model['error']['text'] = "Une erreur est survenue.";
        }
    }
    echo $twig->render('coupon/insert.html.twig', array('model' => $model));
}

function actionCouponUpdate($twig, $db)
{
    $model = array();
    $coupon = new Coupon($db);
    $model['error']['status'] = 1;
    if (isset($_GET['id'])) {
        $coup = $coupon->get($_GET['id']);
        if (empty($coup)) {
            $model['error']['text'] = "Le coupon n'existe pas.";
        } else {
            unset($model['error']['status']);
            if (isset($_POST['libelle'])) {
                $model['error'] = array();
                var_dump($coupon->update($_GET['id'], $_POST['libelle'], $_POST['description'], $_POST['date']));
                if ($coupon->update($_GET['id'], $_POST['libelle'], $_POST['description'], $_POST['date'])) {
                    $model['error']['status'] = 0;
                    $model['error']['text'] = "Le coupon a été modifié.";
                } else {
                    $model['error']['status'] = 1;
                    $model['error']['text'] = "Une erreur est survenue.";
                }
            }
            $model['coupon'] = $coup;
        }
    } else {
        $model['error']['text'] = "Le coupon n'existe pas.";
    }

    echo $twig->render('coupon/update.html.twig', array('model' => $model));
}

//// QRCODE ////

function actionQrcode($twig, $db)
{
    $model = array();
    $qrcode = new Qrcode($db);
    if (isset($_GET['deleted'])) {
        $model['error'] = array();
        $model['error']['status'] = 1;
        if (!empty($qrcode->get($_GET['deleted']))) {
            if ($qrcode->delete($_GET['deleted'])) {
                $model['error']['status'] = 0;
                $model['error']['text'] = "Le QR Code a été supprimé.";
            } else {
                $model['error']['text'] = "Une erreur est survenue.";
            }
        } else {
            $model['error']['text'] = "Le QR code n'existe pas.";
        }
    }
    $model['qrcodes'] = $qrcode->find();
    echo $twig->render('qrcode/index.html.twig', array('model' => $model));
}

function actionQrcodeInsert($twig, $db)
{
    $model = array();
    $qrcode = new Qrcode($db);
    if (isset($_POST['code']) || isset($_GET['random'])) {
        $model['error'] = array();
        if (isset($_GET['random'])) {
            $_POST['code'] = generateCode();
        }
        if ($qrcode->insert($_POST['code'])) {
            $model['error']['status'] = 0;
            $model['error']['text'] = "Le QR code a été créé.";
        } else {
            $model['error']['status'] = 1;
            $model['error']['text'] = "Une erreur est survenue.";
        }
    }
    echo $twig->render('qrcode/insert.html.twig', array('model' => $model));
}

function actionQrcodeUpdate($twig, $db)
{
    $model = array();
    $qrcode = new Qrcode($db);
    $model['error']['status'] = 1;
    if (isset($_GET['id'])) {
        $codeqr = $qrcode->get($_GET['id']);
        if (empty($codeqr)) {
            $model['error']['text'] = "Le QR code n'existe pas.";
        } else {
            $model['qrcode'] = $codeqr;
            unset($model['error']['status']);
            if (isset($_GET['random']) || isset($_POST['code'])) {
                if (isset($_GET['random'])) {
                    $newCode = generateCode();
                }
                if (isset($_POST['code'])) {
                    $newCode = $_POST['code'];
                }
                if ($qrcode->update($_GET['id'], $newCode)) {
                    $model['error']['status'] = 0;
                    $model['error']['text'] = "Le QR code a été modifié.";
                } else {
                    $model['error']['status'] = 1;
                    $model['error']['text'] = "Une erreur est survenue.";
                }
            }
        }
    } else {
        $model['error']['text'] = "Le coupon n'existe pas.";
    }
    echo $twig->render('qrcode/update.html.twig', array('model' => $model));
}

//// LINKED ENTITY ////

function actionAccountcoupon($twig, $db)
{
    $model = array();
    $account = new Account($db);
    $coupon = new Coupon($db);
    $accountcoupon = new Accountcoupon($db);
    if (isset($_GET['deleted'])) {
        $model['error'] = array();
        $model['error']['status'] = 1;
        if (!empty($accountcoupon->get($_GET['deleted']))) {
            if ($accountcoupon->delete($_GET['deleted'])) {
                $model['error']['status'] = 0;
                $model['error']['text'] = "La liaison a été supprimée.";
            } else {
                $model['error']['text'] = "Une erreur est survenue.";
            }
        } else {
            $model['error']['text'] = "Le compte n'existe pas.";
        }
    }
    if (isset($_POST['account']) && isset($_POST['coupon'])) {
        $model['error'] = array();
        $model['error']['status'] = 1;
        if (empty($accountcoupon->getByAccountAndCoupon($_POST['account'], $_POST['coupon']))) {
            if ($accountcoupon->insert($_POST['account'], $_POST['coupon'])) {
                $model['error']['status'] = 0;
                $model['error']['text'] = "La liaison a été effectuée.";
            } else {
                $model['error']['text'] = "Une erreur est survenue.";
            }
        } else {
            $model['error']['text'] = "La liaison est déjà existante.";
        }
    }
    $model['accounts'] = $account->find();
    $model['coupons'] = $coupon->find();
    $model['accountcoupons'] = $accountcoupon->find();
    echo $twig->render('linked/accountcoupon.html.twig', array('model' => $model));
}

function actionCouponqrcode($twig, $db)
{
    $model = array();
    $qrcode = new QRcode($db);
    $coupon = new Coupon($db);
    $couponqrcode = new Couponqrcode($db);
    if (isset($_GET['deleted'])) {
        $model['error'] = array();
        $model['error']['status'] = 1;
        if (!empty($couponqrcode->get($_GET['deleted']))) {
            if ($couponqrcode->delete($_GET['deleted'])) {
                $model['error']['status'] = 0;
                $model['error']['text'] = "La liaison a été supprimée.";
            } else {
                $model['error']['text'] = "Une erreur est survenue.";
            }
        } else {
            $model['error']['text'] = "Le compte n'existe pas.";
        }
    }
    if (isset($_POST['qrcode']) && isset($_POST['coupon'])) {
        $model['error'] = array();
        $model['error']['status'] = 1;
        if (empty($couponqrcode->getByCouponAndQrcode($_POST['qrcode'], $_POST['coupon']))) {
            if ($couponqrcode->insert($_POST['qrcode'], $_POST['coupon'])) {
                $model['error']['status'] = 0;
                $model['error']['text'] = "La liaison a été effectuée.";
            } else {
                $model['error']['text'] = "Une erreur est survenue.";
            }
        } else {
            $model['error']['text'] = "La liaison est déjà existante.";
        }
    }
    $model['qrcodes'] = $qrcode->find();
    $model['coupons'] = $coupon->find();
    $model['couponqrcodes'] = $couponqrcode->find();
    echo $twig->render('linked/couponqrcode.html.twig', array('model' => $model));
}

//// MAINTENANCE (error connexion db) ////

function actionMaintenance($twig)
{
    echo $twig->render('error.html.twig', array());
}
